n = int(input("Enter the number of test cases :"))
for n in range(1,n+1):
    try:
        a,b = map(int,input().split(","))
        print(a//b)
    except (ZeroDivisionError,ValueError):
        print("Integer division or modulo by Zero")