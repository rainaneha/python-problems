def spy_game(nums):
    code = [0,0,7,'x']
    for num in nums:
        if code[0] == num:
            code.pop(0)
    return len(code) == 1

print(spy_game([1,2,0,0,7,4,5,6]))